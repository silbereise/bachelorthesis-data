# Evaluation data and scripts for bachelor thesis

Data repository which belongs to the bachelor thesis **Perfuming Java for learners: an adoption of Code Perfumes for Java** by Jakob Silbereisen,
written at the **Chair of Software Engineering II**.

This repository contains the evaluation data and R scripts for visualization / analysis of that data.

The Code Perfumes developer survey be seen and answered [here](https://docs.google.com/forms/d/e/1FAIpQLSdR57jeU08NPRdAf1rGzJ5O-id5OKpv1b-RAc8VGLFs9QwtSw/viewform?usp=sf_link),
but the answers that were considered for the thesis were frozen at 2023-09-09 15:00 CEST.

## Acknowledgments

First and foremost, I would like to express my deepest gratitude to the chair of Software Engineering II, especially my supervisor Prof. Dr. Gordon Fraser and my advisor Philipp Straubinger, for their continuous support throughout the entire process, from the topic selection until the final submission, for having an open ear to my questions, for their patience and the invaluable feedback. Also, a special thank you for providing the anonymized student projects that I could use for gathering data about Perfumes.
		
Many thanks to the **FOCONIS AG** for enabling me to conduct the developer survey with a subset of their developers.
